<?php

namespace app\controllers;

use Yii;
use app\models\Loan;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoanSearch;
use app\models\User;

class LoanController extends Controller {
	public function behaviors() {
		$this->enableCsrfValidation = false;
		
		return [ 
				'verbs' => [ 
						'class' => VerbFilter::className (),
						'actions' => [ 
								'delete' => [ 
										'get',
										'post' 
								] 
						] 
				] 
		];
	}
	/**
	 * display loan view page
	 *
	 * @return array loan details to gridview
	 */
	public function actionLoans() {
		$model = new LoanSearch ();
		$dataprovider = $model->search ( Yii::$app->request->queryParams );
		return $this->render ( 'loans', [ 
				'model' => $model,
				'dataprovider' => $dataprovider 
		] );
	}
	/**
	 * Displays a single Loan model.
	 *
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionView($id) {
		$loan = $this->findModel ( $id );
		// var_dump ( $loan );
		return $this->render ( 'view', [ 
				'model' => $loan,
				'user' => $loan->user 
		] );
	}
	/**
	 * Creates a new Loan model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Loan ();
		$valPost = Yii::$app->request->post ();
		if (count ( $valPost ) !== 0) {
			$user_id = $valPost ['Loan'] ['user_id'];
			$tempModel = User::findOne ( $user_id );
			if ($tempModel === null) {
				/* user id not present hence cannot create loan */
				return $this->render ( 'create', [ 
						'model' => $model,
						'showUserId' => true,
						'errorMsg' => 'User Id not available. Please Create User First' 
				] );
			} else {
				if ($model->load ( Yii::$app->request->post () ) && $model->save ()) {
					/* successfull creation of loan redirect to view page */
					return $this->redirect ( [ 
							'view',
							'id' => $model->id 
					] );
				} else {
					/* unsuccessfull creation of loan renders to same page */
					return $this->render ( 'create', [ 
							'model' => $model,
							'showUserId' => true,
							'errorMsg' => '' 
					] );
				}
			}
		} else {
			/* creating new loan */
			return $this->render ( 'create', [ 
					'model' => $model,
					'showUserId' => true,
					'errorMsg' => '' 
			] );
		}
	}
	/**
	 * Updates an existing Loan model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel ( $id );
		if ($model->load ( Yii::$app->request->post () ) && $model->save ()) {
			/* successfully updation of loan redirect to view page */
			return $this->redirect ( [ 
					'view',
					'id' => $model->id 
			] );
		} else {
			/* unsuccessfull updation of loan renders the same page */
			return $this->render ( 'update', [ 
					'model' => $model,
					'showUserId' => true,
					'errorMsg' => '' 
			] );
		}
	}
	
	/**
	 * Deletes an existing Loan model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionDelete($id) {
		$user = $this->findModel ( $id );
		$user->delete ();
		return $this->redirect ( [ 
				'loans' 
		] );
	}
	/**
	 * Finds the Loan model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id        	
	 * @return Loan the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Loan::findOne ( $id )) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
}