<?php

namespace app\controllers;

use Yii;
use app\models\User;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\models\UserSearch;

class UserController extends Controller {
	public function behaviors() {
		$this->enableCsrfValidation = false;
		return [ 
				'verbs' => [ 
						'class' => VerbFilter::className (),
						'actions' => [ 
								'delete' => [ 
										'post',
										'get' 
								] 
						] 
				] 
		];
	}
	/**
	 * Displays user page
	 *
	 * @return array user details gridview
	 *        
	 */
	public function actionUsers() {
		$model = new UserSearch ();
		$dataprovider = $model->search ( Yii::$app->request->queryParams );
		return $this->render ( 'users', [ 
				'model' => $model,
				'dataprovider' => $dataprovider 
		] );
	}
	/**
	 * Displays a single User model.
	 *
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionView($id) {
		$user = $this->findModel ( $id );
		return $this->render ( 'view', [ 
				'model' => $user,
				'userAge' => $user->getAge (),
				'loans' => $user->loans 
		] );
	}
	/**
	 * Creates a new User model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new User ();
		if ($model->load ( Yii::$app->request->post () ) && $model->save ()) {			
			return $this->redirect ( [ 
					'view',
					'id' => $model->id 
			] );
		} else {
			return $this->render ( 'create', [ 
					'model' => $model 
			] );
		}
	}
	
	/**
	 * Updates an existing User model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel ( $id );
		
		if ($model->load ( Yii::$app->request->post () ) && $model->save ()) {
			return $this->redirect ( [ 
					'view',
					'id' => $model->id 
			] );
		} else {
			return $this->render ( 'update', [ 
					'model' => $model 
			] );
		}
	}
	
	/**
	 * Deletes an existing User model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionDelete($id) {
		$user = $this->findModel ( $id );
		$user->delete ();
		return $this->redirect ( [ 
				'users' 
		] );
	}
	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id        	
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = User::findOne ( $id )) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
}