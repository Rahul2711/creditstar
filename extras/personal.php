<?php

namespace app\extras;

abstract class personal {
	/**
	 * personal code validate
	 *
	 * @return bool
	 */
	public static function validatePersonalCode($id) {
		$prcode = substr ( $id, 10, 1 );
		
		$nr = ' ' . $id;
		$genChecksum = (( int ) $nr [1] + ( int ) $nr [2] * 2 + ( int ) $nr [3] * 3 + ( int ) $nr [4] * 4 + ( int ) $nr [5] * 5 + ( int ) $nr [6] * 6 + ( int ) $nr [7] * 7 + ( int ) $nr [8] * 8 + ( int ) $nr [9] * 9 + ( int ) $nr [10]) % 11;
		$genChecksum = ( int ) $genChecksum;
		if ($genChecksum == 10) {
			$genChecksum = (( int ) $nr [1] * 3 + ( int ) $nr [2] * 4 + ( int ) $nr [3] * 5 + ( int ) $nr [4] * 6 + ( int ) $nr [5] * 7 + ( int ) $nr [6] * 8 + ( int ) $nr [7] * 9 + ( int ) $nr [8] + ( int ) $nr [9] * 2 + ( int ) $nr [10] * 3) % 11;
		}
		
		$aPersonalCodeData = self::getPersonalCodeData ( $id );
		
		// var_dump ( ltrim ( $nr ) );
		
		// var_dump ( $nr [1] );
		// var_dump ( $genChecksum );
		// var_dump ( $prcode );
		
		if (strlen ( ltrim ( $nr ) ) == 11 && ( int ) $nr [1] != 0 && ( int ) $genChecksum == ( int ) $prcode && $aPersonalCodeData ['birthdate_correct'] === true) {
			return true;
		}
	}
	
	/**
	 * recive personal code data
	 *
	 * @return array
	 */
	public static function getPersonalCodeData($iPersonalCode) {
		$idp = unpack ( "a1gender/a2year/a2month/a2day/a3ord/a1chk", $iPersonalCode ); // split $id into chunks (id parts)
		
		$aData ['gender'] = (( int ) $idp ['gender'] % 2) ? "M" : "F"; // gender
		$aData ['b_year'] = (17 + ( int ) ((( int ) $idp ['gender'] + 1) / 2)) . ( int ) $idp ['year']; // year of birth
		$aData ['b_month'] = ( int ) $idp ['month']; // month of birth
		$aData ['b_day'] = ( int ) $idp ['day']; // day of birth
		
		$aData ['birthdate'] = $aData ['b_year'] . '-' . str_pad ( $aData ['b_month'], 2, '0', STR_PAD_LEFT ) . '-' . str_pad ( $aData ['b_day'], 2, '0', STR_PAD_LEFT );
		
		$aData ['birthdate_correct'] = checkdate ( $aData ['b_month'], $aData ['b_day'], $aData ['b_year'] );
		
		$date1 = new \DateTime ( $aData ['birthdate'] );
		$date2 = new \DateTime ();
		$interval = $date1->diff ( $date2 );
		$aData ['age'] = $interval->y;
		
		return $aData;
	}
	
	/**
	 * recive age
	 *
	 * @return array
	 */
	public static function getAgeByPersonalCode($perCode) {
		$aPersonalCodeData = self::getPersonalCodeData ( $perCode );
		return $aPersonalCodeData ['age'];
	}
}