<?php
use yii\db\Migration;
class m190810_062934_insertData extends Migration {
	public function safeUp() {
		/* migrating users data to database */
		$users = ( array ) json_decode ( file_get_contents ( __DIR__ . '/../users.json' ) );
		$fuser = array_keys ( ( array ) $users [0] );
		
		$aRows = array ();
		
		foreach ( $users as $val ) {
			$user_rows [] = array_values ( ( array ) $val );
		}
		
		$this->batchInsert ( 'user', $fuser, $user_rows );
		
		/* migrating loans data to database */
		$loans = ( array ) json_decode ( file_get_contents ( __DIR__ . '/../loans.json' ) );
		$floan = array_keys ( ( array ) $loans [0] );
		
		$aRows = array ();
		
		foreach ( $loans as $val ) {
			$userloan = ( array ) $val;
			
			$userloan ['start_date'] = date ( "Y-m-d", $userloan ['start_date'] );
			$userloan ['end_date'] = date ( "Y-m-d", $userloan ['end_date'] );
			
			$loan_rows [] = array_values ( $userloan );
		}
		$this->batchInsert ( 'loan', $floan, $loan_rows );
	}
	public function safeDown() {
		// drop foreign key before the tables will be truncated
		$aForeignKeys = Yii::$app->db->schema->getTableSchema ( 'loan', true )->foreignKeys;
		
		if (isset ( $aForeignKeys [0] ['user_id'] )) {
			$this->dropForeignKey ( 'loanFakeuserID', 'loan' );
		}
		
		$this->truncateTable ( 'loan' );
		$this->truncateTable ( 'user' );
		
		// add foreign key
		$this->addForeignKey ( 'loanFakeuserID', 'loan', 'user_id', 'user', 'id', 'RESTRICT', 'CASCADE' );
	}
}
