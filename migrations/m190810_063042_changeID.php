<?php
use yii\db\Migration;

/**
 * Class m190810_063042_changeID
 */
class m190810_063042_changeID extends Migration {
	public function safeUp() {
		$iMaxUserId = (new \yii\db\Query ())->from ( 'user' )->max ( '"id"' );
		
		$this->execute ( 'ALTER SEQUENCE "Users_userId_seq" RESTART WITH ' . ++ $iMaxUserId );
		
		$iMaxLoanId = (new \yii\db\Query ())->from ( 'loan' )->max ( '"id"' );
		
		$this->execute ( 'ALTER SEQUENCE "Loans_loanId_seq" RESTART WITH ' . ++ $iMaxLoanId );
	}
	public function safeDown() {
		$this->execute ( 'ALTER SEQUENCE "Users_userId_seq" RESTART' );
		
		$this->execute ( 'ALTER SEQUENCE "Loans_loanId_seq" RESTART' );
	}
}
