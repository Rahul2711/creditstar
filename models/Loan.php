<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Loan extends ActiveRecord {
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'loan';
	}
	public function rules() {
		return [ 
				[ 
						[ 										
								'user_id',
								'amount',
								'interest',
								'duration',
								'start_date',
								'end_date',
								'campaign',
								'status' 
						],
						'required' 
				],
				[ 
						[ 								
								'user_id',
								'duration',
								'campaign',
								'status' 
						],
						'integer' 
				],
				[ 
						[ 
								'amount',
								'interest' 
						],
						'double' 
				],
				[ 
						[ 
								'start_date',
								'end_date' 
						],
						'match',
						'pattern' => '/^\d\d\d\d-\d\d-\d\d$/' 
				] 
		];
	}
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne ( User::className (), [ 
				'id' => 'user_id' 
		] );
	}
}