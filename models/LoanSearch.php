<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Loan;

/**
 * LoanSearch represents the model behind the search form about `app\models\Loan`.
 */
class LoanSearch extends Loan {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'id',
								'user_id',
								'duration',
								'campaign',
								'status' 
						],
						'integer' 
				],
				[ 
						[ 
								'amount',
								'interest' 
						],
						'number' 
				],
				[ 
						[ 
								'start_date',
								'end_date' 
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Loan::find ();
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'pagination' => [ 
						'pageSize' => 10 
				],
				'sort' => [ 
						'defaultOrder' => [ 
								'start_date' => SORT_DESC 
						] 
				] 
		] );
		
		$this->load ( $params );		
		if (! $this->validate ()) {
			return $dataProvider;
		}
		
		$query->andFilterWhere ( [ 
				'id' => $this->id,
				'user_id' => $this->user_id,
				'amount' => $this->amount,
				'interest' => $this->interest,
				'duration' => $this->duration,
				'start_date' => ! empty ( $this->start_date ) ? date ( "Y-m-d", strtotime ( $this->start_date ) ) : null,
				'end_date' => ! empty ( $this->end_date ) ? date ( "Y-m-d", strtotime ( $this->end_date ) ) : null 
		] );
		
		return $dataProvider;
	}
}
