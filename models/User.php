<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\extras\personal;

class User extends ActiveRecord {
	// public $id;
	// public $first_name;
	// public $last_name;
	// public $email;
	// public $personal_code;
	// public $phone;
	// public $active;
	// public $dead;
	// public $lang;
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'user';
	}
	public function rules() {
		return [ 
				[ 
						[ 
								'first_name',
								'last_name',
								'email',
								'personal_code',
								'phone',
								'lang' 
						
						],
						'required' 
				],
				[ 
						[ 
								'first_name',
								'last_name',
								'email',
								'lang' 
						],
						'string' 
				],
				[ 
						[ 
								'personal_code',
								'phone' 
						],
						'integer' 
				],
				[ 
						[ 
								'dead',
								'active' 
						],
						'boolean' 
				],
				[ 
						[ 
								'email' 
						],
						'email' 
				],
				[ 
						[ 
								'personal_code' 
						],
						'validatePersonalCode' 
				],
				[ 
						[ 
								'personal_code' 
						],
						'unique',
						'targetAttribute' => 'personal_code' 
				] 
		];
	}
	/**
	 * get loan details
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getLoans() {
		return $this->hasMany ( Loan::className (), [ 
				'user_id' => 'id' 
		] )->inverseOf ( 'user' );
	}
	/**
	 * validate personal code
	 */
	public function validatePersonalCode($attribute, $value) {
		$validate = personal::validatePersonalCode ( $this->$attribute );
		if ($validate !== true) {
			$this->addError ( $attribute, 'Personal code is Invalid' );
		}
	}
	/**
	 * get user age
	 *
	 * @return array
	 */
	public function getAge() {
		return personal::getAgeByPersonalCode ( $this->personal_code );
	}
}