<?php
$this->title = 'Create Loan';
$this->params ['breadcrumbs'] [] = [ 
		'label' => 'Loans',
		'url' => [ 
				'loans' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div>	
    <?=$this->render ( 'loansedit', [ 'model' => $model,'showUserId' => $showUserId,'errorMsg'=>$errorMsg] )?>
</div>
