<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

$this->title = 'Loans';
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div>
	<h1><?= Html::encode($this->title) ?></h1>
	<div class="body-content">
		<div class="row">
			<div class="col-lg-12">
		 <?php
			$form = ActiveForm::begin ( [ 
					'action' => [ 
							'loans' 
					],
					'method' => 'get' 
			] );
			?>
     <p>
        <?= Html::a('Create Loan', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
     </p>
    <?=GridView::widget ( [ 'dataProvider' => $dataprovider,'filterModel' => $model,'columns' => [ [ 'class' => 'yii\grid\SerialColumn' ],'id','user_id','amount','interest','duration',[ 'attribute' => 'start_date','format' => [ 'date','php:d.m.Y' ] ],[ 'class' => 'yii\grid\ActionColumn' ] ] ] );?>
    <?php ActiveForm::end(); ?>
           </div>
		</div>
	</div>
</div>