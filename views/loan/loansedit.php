<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$sValue = Yii::$app->request->get ( 'id' );
?>

<div class="panel-group">
	<div class="panel panel-warning">
		<div class="panel-heading"><?= Html::encode($this->title) ?></div>
		<div class="panel-body">
		   <?php $form = ActiveForm::begin(['id' => 'loans-edit']); ?>
		   <?php if ($showUserId === false) {?>		     
			  <?=$form->field ( $model, 'user_id' )->textInput ( [ 'readonly' => 'readonly','maxlength'=>20 ] )->label ( 'User Id' )?>                              
           <?php }else{?>
              <?php if(!empty($sValue)){?>        
              <?= $form->field($model, 'user_id')->textInput ( [ 'readonly' => 'readonly','maxlength'=>20,'value'=>$sValue ] )->label ( 'User Id' )?>
              <?php }else{?>
              <?= $form->field($model, 'user_id')->label('User Id')->textInput(['maxlength'=>20])?>
              <?php }?>                                                           
           <?php }?>
           
              <?= $form->field($model, 'amount')->label('Amount')->textInput(['maxlength'=>20])?>
              
              <?= $form->field($model, 'interest')->label('Interest')->textInput(['maxlength'=>20])?>  
              
              <?= $form->field($model, 'duration')->label('Duration')->textInput(['maxlength'=>4])?>                 

              <?=$form->field ( $model, 'start_date' )->label('Start Date')->textInput(['maxlength'=>10,'type'=>'date'])?>
              
              <?=$form->field ( $model, 'end_date' )->label('End Date')->textInput(['maxlength'=>10,'type'=>'date'])?>                                                                                                                           
                                       
              <?=$form->field($model, "campaign")->label('Campaign')->textInput(['maxlength'=>2])?>
               
              <?=$form->field($model, "status")->label('Status')->textInput(['maxlength'=>2])?>
              
              <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-danger']) ?>
               
               <label><?php echo $errorMsg; ?></label>        
              <?php  ActiveForm::end(); ?> 
		</div>
	</div>
</div>
