<?php
$this->title = 'Update Loan : ' . ' ' . $model->id;
$this->params ['breadcrumbs'] [] = [ 
		'label' => 'Loans',
		'url' => [ 
				'loans' 
		] 
];
$this->params ['breadcrumbs'] [] = [ 
		'label' => $model->id,
		'url' => [ 
				'view',
				'id' => $model->id 
		] 
];
$this->params ['breadcrumbs'] [] = 'Update';
?>
<div>	
    <?=$this->render ( 'loansedit', [ 'model' => $model,'showUserId' => $showUserId,'errorMsg' => $errorMsg ] )?>
</div>
