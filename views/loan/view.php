<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
$this->params ['breadcrumbs'] [] = [ 
		'label' => 'Loans',
		'url' => [ 
				'loans' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;

?>
<div>
	<h1>Loan <?= Html::encode($this->title) ?></h1>
	<div>
		<p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            
            <?=Html::a ( 'Delete', [ 'delete','id' => $model->id ], [ 'class' => 'btn btn-danger','data' => [ 'confirm' => 'Are you sure you want to delete this item?','method' => 'post' ] ] )?>
        </p>

        <?=DetailView::widget ( [ 'model' => $model,'attributes' => [ 'id',[ 'label' => 'User ID','format' => 'raw','value' => Html::a ( $model->user_id, [ 'user/view','id' => $model->user_id ] ) ],[ 'label' => 'Personal Code','value' => $user->personal_code ],[ 'label' => 'First Name','value' => $user->first_name ],[ 'label' => 'Last Name','value' => $user->last_name ],'amount','interest','duration',[ 'attribute' => 'start_date','format' => [ 'date','php:d.m.Y' ] ],[ 'attribute' => 'end_date','format' => [ 'date','php:d.m.Y' ] ],'campaign','status' ] ] )?>
        
    </div>

</div>
