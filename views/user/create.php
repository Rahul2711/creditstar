<?php
$this->title = 'Create User';
$this->params ['breadcrumbs'] [] = [ 
		'label' => 'Users',
		'url' => [ 
				'users' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div>
    <?=$this->render ( 'usersedit', [ 'model' => $model ] )?>
</div>
