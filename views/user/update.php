<?php
$this->title = 'Update User : ' . ' ' . $model->id;
$this->params ['breadcrumbs'] [] = [ 
		'label' => 'Users',
		'url' => [ 
				'users' 
		] 
];
$this->params ['breadcrumbs'] [] = [ 
		'label' => $model->id,
		'url' => [ 
				'view',
				'id' => $model->id 
		] 
];
$this->params ['breadcrumbs'] [] = 'Update';
?>
<div>	
    <?=$this->render ( 'usersedit', [ 'model' => $model ] )?>
</div>
