<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
$this->title = 'Users';
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="site-about">
	<h1><?= Html::encode($this->title) ?></h1>

	<div class="body-content">
		<div class="row">
			<div class="col-lg-12">
			 <?php
				$form = ActiveForm::begin ( [ 
						'action' => [ 
								'users' 
						],
						'method' => 'get' 
				] );
				?>
    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </p>
    <?=GridView::widget ( [ 'dataProvider' => $dataprovider,'filterModel' => $model,'columns' => [ [ 'class' => 'yii\grid\SerialColumn' ],'id','first_name:ntext','last_name:ntext','personal_code','phone',[ 'class' => 'yii\grid\ActionColumn' ] ] ] );?>
    <?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
