<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="panel-group">
	<div class="panel panel-warning">
		<div class="panel-heading"><?= Html::encode($this->title) ?></div>
	  <?php $form = ActiveForm::begin(['id' => 'user-edit']); ?>
		<div class="panel-body">              
              <?= $form->field($model, 'first_name')->label('First Name')->textInput(['style' => '', 'maxlength' => 255])?>
              
              <?= $form->field($model, 'last_name')->label('Last Name')->textInput(['style' => '', 'maxlength' => 255])?>                    

              <?= $form->field($model, 'email')->label('E-mail')->textInput(['style' => '', 'maxlength' => 255])?>                   

              <?=$form->field ( $model, 'phone' )->label('Phone Number')->textInput(['style' => '', 'maxlength' => 20])?>                                                                                                                                                     			              
              
              <?= $form->field($model, 'personal_code')->label('Personal Code')->textInput(['style' => '', 'maxlength' => 11])?>
              
              <?=$form->field ( $model, 'lang' )->label('Language')->dropDownList (['est' => 'Estonian', 'rus' => 'Russian','eng'=>'English'],['prompt'=>'Select Language'])?>
              
              <?=$form->field($model, 'active')->checkbox();?>
              
              <?=$form->field($model, "dead")->checkbox();?>              
              
              <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-danger']) ?>
        </div>
         <?php ActiveForm::end(); ?>
	</div>
</div>
