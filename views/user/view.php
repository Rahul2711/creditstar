<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
$this->title = $model->id;
$this->params ['breadcrumbs'] [] = [ 
		'label' => 'Users',
		'url' => [ 
				'users' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div>
	<h1>User <?= Html::encode($this->title) ?></h1>
	<div>
		<p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            
            <?=Html::a ( 'Delete', [ 'delete','id' => $model->id ], [ 'class' => 'btn btn-danger','data' => [ 'confirm' => 'Are you sure you want to delete this user?','method' => 'post' ] ] )?>
            
            <?= Html::a('Create Loan', ['loan/create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        </p>

        <?=DetailView::widget ( [ 'model' => $model,'attributes' => [ 'id','personal_code','first_name:ntext','last_name:ntext',[ 'label' => 'Age','value' => $userAge ],'email:email','phone','active:boolean','dead:boolean','lang:ntext',[ 'label' => 'Loans count','format' => 'raw','value' => Html::a ( count ( $loans ), [ 'loan/loans','LoanSearch[user_id]' => $model->id ] ) ] ] ] )?>
    </div>
</div>
